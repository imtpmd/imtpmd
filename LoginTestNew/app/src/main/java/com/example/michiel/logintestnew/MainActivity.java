package com.example.michiel.logintestnew;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Boolean isFirstRun = getSharedPreferences("PREFS", MODE_PRIVATE)
                .getBoolean("isFirstRun", true);

        if (isFirstRun) {
            //show start activity

            startActivity(new Intent(MainActivity.this, FirstTimeActivity.class));
        }

        getSharedPreferences("PREFS", MODE_PRIVATE).edit()
                .putBoolean("isFirstRun", false).commit();

        //Intent intent = getIntent();
        //String studentName = intent.getStringExtra("Name");

        String studentName = getSharedPreferences("PREFS", MODE_PRIVATE)
                .getString("Name", "Test");

        TextView tv = (TextView) findViewById(R.id.textView2);
        tv.setText("Welkom " + studentName + "!");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_home) {
            startActivity(new Intent(this, MainActivity.class));
        } else if (id == R.id.action_progress) {
            startActivity(new Intent(this, ProgressActivity.class));
        } else if (id == R.id.action_visualisation) {
            startActivity(new Intent(this, VisualisationActivity.class));
        } else if (id == R.id.action_advice) {
            startActivity(new Intent(this, AdviceActivity.class));
        } else if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }
}
