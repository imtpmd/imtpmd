package michielendaniel.imtpmd;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {
    EditText etNaam;
    SharedPref s;
    Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        s = new SharedPref();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginButton = (Button) findViewById(R.id.btn_login);
        etNaam = (EditText) findViewById(R.id.login_naam);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!etNaam.getText().toString().equals("")){
                    s.saveString(LoginActivity.this, etNaam.getText().toString(), "NAME");
                    s.saveBoolean(LoginActivity.this, false, "FIRST");
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));

                }else{
                    new AlertDialog.Builder(LoginActivity.this)
                            .setTitle(R.string.enterNameTitle)
                            .setMessage(R.string.enterName)
                            .setPositiveButton(R.string.understood, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).show();
                }
            }
        });

    }

}
