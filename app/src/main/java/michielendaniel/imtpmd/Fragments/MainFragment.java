package michielendaniel.imtpmd.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;

import java.util.ArrayList;

import michielendaniel.imtpmd.Database.DatabaseHelper;
import michielendaniel.imtpmd.Database.DatabaseInfo;
import michielendaniel.imtpmd.R;
import michielendaniel.imtpmd.SharedPref;

public class MainFragment extends Fragment {
    SharedPref s = new SharedPref();
    private PieChart mChart;
    ProgressBar progressBar;
    TextView puntenTV;
    TextView welkomTV;
    public static int currentEcts;
    DatabaseHelper dbHelper;
    Cursor rs;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        dbHelper = DatabaseHelper.getHelper(getActivity());
        rs = dbHelper.query(DatabaseInfo.CourseTables.COURSE, new String[]{"*"}, null, null, null, null, null);

        setStudiepunten();

        View view = inflater.inflate(R.layout.fragment_main, container, false);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // if user clicked on back, select first menu item
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        new AlertDialog.Builder(getActivity())
                                .setTitle(R.string.leaveApp)
                                .setMessage(R.string.leaveMessage)
                                .setPositiveButton(R.string.positiveButton, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        System.exit(1);
                                    }
                                })
                                .setNegativeButton(R.string.negativeButton, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        //Do nothing
                                    }
                                })
                                .show();
                        return true;
                    }
                }
                return false;
            }
        });
        progressBar = (ProgressBar) view.findViewById(R.id.home_progressbar);
        puntenTV = (TextView) view.findViewById(R.id.home_aantal_punten);
        welkomTV = (TextView) view.findViewById(R.id.home_welkom);
        currentEcts = s.getInteger(getActivity(), "CURRENT_ECTS");
        String userName = s.getString(getActivity(), "NAME");
        puntenTV.setText(currentEcts + " van de 60 punten behaald");
        welkomTV.setText("Welkom " + userName + "!");
        progressBar.setProgress(currentEcts);

        mChart = (PieChart) view.findViewById(R.id.chart);
        mChart.setDescription("");
        mChart.setTouchEnabled(false);
        mChart.setDrawSliceText(true);
        mChart.getLegend().setEnabled(false);
        mChart.setTransparentCircleColor(Color.rgb(130, 130, 130));
        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        setData(currentEcts);
        return view;
    }

    private void setData(int aantal) {
        currentEcts = aantal;
        ArrayList<Entry> yValues = new ArrayList<>();
        ArrayList<String> xValues = new ArrayList<>();

        yValues.add(new Entry(aantal, 0));
        xValues.add("Behaalde EC's");

        yValues.add(new Entry(60 - currentEcts, 1));
        xValues.add("Te behalen EC's");

        ArrayList<Integer> colors = new ArrayList<>();
        if (currentEcts < 40){
            colors.add(Color.rgb(235,0,0));
        } else if  (currentEcts < 50) {
            colors.add(Color.rgb(255,165,0));
        } else {
            colors.add(Color.rgb(0, 255, 0));
        }
        colors.add(Color.rgb(34, 17, 75));

        PieDataSet dataSet = new PieDataSet(yValues, "ECTS");
        dataSet.setColors(colors);

        PieData data = new PieData(xValues, dataSet);
        mChart.setData(data); // bind dataset aan chart.
        mChart.invalidate();  // Aanroepen van een redraw
    }

    public void setStudiepunten(){
        int studiepunten = 0;

        if(rs != null && rs.moveToFirst()) {
            for (int i = 0; i < 19; i++) {

                String ects = rs.getString(rs.getColumnIndex("ects"));
                String cijfer = rs.getString(rs.getColumnIndex("grade"));
                if (Double.parseDouble(cijfer) >= 5.5) {
                    studiepunten += Integer.parseInt(ects);
                }
                rs.moveToNext();
            }
        }

        s.saveInteger(getActivity(), studiepunten, "CURRENT_ECTS");
    }
}
