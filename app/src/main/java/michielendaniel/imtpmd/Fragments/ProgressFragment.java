package michielendaniel.imtpmd.Fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import michielendaniel.imtpmd.CourseActivity;
import michielendaniel.imtpmd.Database.DatabaseHelper;
import michielendaniel.imtpmd.Database.DatabaseInfo;
import michielendaniel.imtpmd.GSON.GsonRequest;
import michielendaniel.imtpmd.GSON.VolleyHelper;
import michielendaniel.imtpmd.List.CourseListAdapter;
import michielendaniel.imtpmd.Model.Course;
import michielendaniel.imtpmd.R;
import michielendaniel.imtpmd.SharedPref;

public class ProgressFragment extends Fragment {
    SharedPref s;

    private ListView mListView;
    private CourseListAdapter mAdapter;
    private List<Course> course = new ArrayList<>();

    Cursor rs;
    DatabaseHelper dbHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_progress, container, false);
        view.setFocusableInTouchMode(true);
        view.requestFocus();

        dbHelper = DatabaseHelper.getHelper(getActivity());
        rs = dbHelper.query(DatabaseInfo.CourseTables.COURSE, new String[]{"*"}, null, null, null, null, null);

        mListView = (ListView) view.findViewById(R.id.my_list_view);
        mAdapter = new CourseListAdapter(getActivity(), 0, course);

        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // if user clicked on back, select first menu item
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        FragmentManager fm = getFragmentManager();
                        setTargetFragment(new MainFragment(), 1);
                        fm.beginTransaction().replace(R.id.content_frame, getTargetFragment());
                        return true;
                    }
                }
                return false;
            }
        });

        try {
            s = new SharedPref();
            requestSubjects();

        }catch(Exception e){}

        return view;
    }

    private void requestSubjects(){
        Type type = new TypeToken<List<Course>>(){}.getType();
        GsonRequest<List<Course>> request = new GsonRequest<List<Course>>(
                "http://fuujokan.nl/subject_lijst.json", type, null,
                new Response.Listener<List<Course>>() {
                    @Override
                    public void onResponse(List<Course> response) {
                        processRequestSucces(response);
                        Log.e("Repsonse", response.toString());
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){ processRequestError(error);     }
        }
        );
        VolleyHelper.getInstance(getActivity()).addToRequestQueue(request);
    }

    private void processRequestSucces(List<Course> subjects ){
        ContentValues values = new ContentValues();
        for (Course c : subjects) {
            values.put(DatabaseInfo.CourseColumn.NAME, c.getCourseName());
            values.put(DatabaseInfo.CourseColumn.ECTS, c.getEcts());
            values.put(DatabaseInfo.CourseColumn.PERIOD, c.getPeriod());
            values.put(DatabaseInfo.CourseColumn.GRADE, c.getGrade());
            dbHelper.insert(DatabaseInfo.CourseTables.COURSE, null, values);
        }

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(view.getContext(), CourseActivity.class);
                intent.putExtra("position", position);
                startActivity(intent);
            }
        });

        if(rs != null && rs.moveToFirst()) {
            for (Course c : subjects) {
                String name = (String) rs.getString(rs.getColumnIndex("name"));
                String ects = (String) rs.getString(rs.getColumnIndex("ects"));
                String grade = (String) rs.getString(rs.getColumnIndex("grade"));
                String period = (String) rs.getString(rs.getColumnIndex("period"));
                course.add(new Course(name, ects, grade, period));
                Log.e("Values", name.toString());
                rs.moveToNext();
            }
        }

        rs.moveToFirst();
        mListView.setAdapter(mAdapter);
    }

    private void processRequestError(VolleyError error){  }

}





