package michielendaniel.imtpmd.Fragments;

import android.app.FragmentManager;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import michielendaniel.imtpmd.Database.DatabaseHelper;
import michielendaniel.imtpmd.Database.DatabaseInfo;
import michielendaniel.imtpmd.Model.Course;
import michielendaniel.imtpmd.R;
import michielendaniel.imtpmd.SharedPref;

public class AdviceFragment extends Fragment {

    TextView adviesAlgemeen;
    TextView adviesECTS;
    TextView adviesHoofdvakken;
    TextView adviesSpecvakken;

    Calendar cal = Calendar.getInstance();
    Calendar Periode1;
    Calendar Periode2;
    Calendar Periode3;
    Calendar Periode4;

    int currentDay;
    int currentMonth;
    int currentYear;
    int currentPeriod;

    SharedPref s;

    Cursor rs;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        try{
            DatabaseHelper dbHelper = DatabaseHelper.getHelper(getActivity());
             rs = dbHelper.query(DatabaseInfo.CourseTables.COURSE, new String[]{"*"}, null, null, null, null, null);
        }catch(Exception e){}
        View view = inflater.inflate(R.layout.fragment_advice, container, false);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // if user clicked on back, select first menu item
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        FragmentManager fm = getFragmentManager();
                        setTargetFragment(new MainFragment(), 1);
                        fm.beginTransaction().replace(R.id.content_frame, getTargetFragment());
                        return true;
                    }
                }
                return false;
            }
        });
        s = new SharedPref();

        adviesAlgemeen = (TextView) view.findViewById(R.id.textView_advies_algemeen);
        adviesECTS = (TextView) view.findViewById(R.id.textView_advies_studiepunten);
        adviesHoofdvakken = (TextView) view.findViewById(R.id.textView_advies_verplichte_vakken);
        adviesSpecvakken = (TextView) view.findViewById(R.id.textView_advies_specialisatie_vakken);

        currentDay = cal.get(Calendar.DAY_OF_MONTH);
        currentMonth = cal.get(Calendar.MONTH);
        currentMonth += 1;
        currentYear = cal.get(Calendar.YEAR);

        currentPeriod = getCurrentPeriod();

        setAlgemeneTekst();
        setStudiepuntenTekst();
        setHoofdvakkenTekst();
        setSpecvakkenTekst();
        return view;
    }

    public void setAlgemeneTekst(){
        String tekst = "Beste " + s.getString(getActivity(), "NAME") + ", \n" +
                "Je zit momenteel in periode " + currentPeriod + " en hebt nu " + s.getInteger(getActivity(), "CURRENT_ECTS") + " studiepunten behaald. \n" +
                "Aan het einde van deze periode kun je maximaal " + maxPuntenPeriode(currentPeriod) + " studiepunten behaald hebben. \n" +
                "In onderstaande alinea's krijg je advies op basis van je studiepunten, de hoofdvakken en de specialisatievakken. \n \n " +
                "Let op: Dit advies is niet bindend, bespreek dit advies met je SLB'er!";
        adviesAlgemeen.setText(tekst);
    }
    public void setStudiepuntenTekst(){
        String tekst = "Je hebt momenteel " + s.getInteger(getActivity(), "CURRENT_ECTS") + " studiepunten behaald, waar je er aan het einde van de periode " + maxPuntenPeriode(currentPeriod) +
                " kan behalen. \n" + "Op dit moment mis je dus " + missendePunten() + " punten (punten uit de vorige perioden) en kan je dit jaar nog maximaal " + (60 - missendePunten()) + " studiepunten behalen. \n" +
                "\n" + geefStudiepuntenAdvies();
        adviesECTS.setText(tekst);
    }
    public void setHoofdvakkenTekst(){
        //Bereken of hoofdvakken zijn behaald of niet behaald (of nog niet gedaan)
        //Meer dan 2 hoofdvakken onvoldoende? Ah.. oh..
        //Set text
        String tekst = "Om naar het tweede jaar te mogen moet je minimaal twee van de volgende vakken behaald hebben: \n" +
                "IOPR1, IOPR2, IRDB en INET \n \n" +
                "Je hebt momenteel " + behaaldeHoofdvakken() + " verplicht(e) vak(ken) behaald" + geefHoofdvakkenAdvies();
        adviesHoofdvakken.setText(tekst);
    }
    public void setSpecvakkenTekst(){
        //Bereken of spec vakken zijn behaald
        /*Hoofdvakken:
        * Medt - IWDR
        * BDaM - IIBUI (?)
        * FICT - IFIT
        * SE - IMUML
         */
        //set text
        String tekst = "Voor iedere specialisatie is er een verplicht vak. Als je dit vak haalt, mag je volgend jaar deze specialisatie doen (mits je genoeg punten hebt!) \n" +
                "Voor MediaTechnologie is het verplichte vak IWDR, voor Business en Data Management is dat IIBUI, voor Forensisch IT is dat IFIT en voor Software Engineering heb je IMUML nodig. \n \n" +
                medtSpec() +"\n"+ bdamSpec() +"\n"+ fictSpec() +"\n"+ seSpec();
        adviesSpecvakken.setText(tekst);
    }

    public int getCurrentPeriod(){
        /*Einde periode 1: 15-11
        * Einde periode 2: 7-2
        * Einde periode 3: 24-4
        * Einde periode 4: 17-7
        */
        Periode1 = Calendar.getInstance();
        Periode2 = Calendar.getInstance();
        Periode3 = Calendar.getInstance();
        Periode4 = Calendar.getInstance();

        Periode1.set(2015, 11, 15);
        Periode2.set(2016, 2, 7);
        Periode3.set(2016, 4, 24);
        Periode4.set(2016, 7, 17);

        int periode = 0;

        //Controleer eerst of het jaar gelijk is en of de maand kleiner is of gelijk aan
        if(Periode1.get(Calendar.YEAR) == currentYear && currentMonth <= Periode1.get(Calendar.MONTH)){
            // Kleiner dan? Valt in deze periode.
            // Gelijk aan? Controleer de dag om te zien of het binnen de periode valt.
            if(currentMonth < Periode1.get(Calendar.MONTH)){
                periode = 1;
            }else if(Periode1.get(Calendar.MONTH) == currentMonth && currentDay < Periode1.get(Calendar.DAY_OF_MONTH)) {
                periode = 1;
            }
        }else if(Periode2.get(Calendar.YEAR) == currentYear && currentMonth <= Periode2.get(Calendar.MONTH)){
            if(currentMonth < Periode2.get(Calendar.MONTH)){
                periode = 2;
            }else if(Periode2.get(Calendar.MONTH) == currentMonth && currentDay < Periode2.get(Calendar.DAY_OF_MONTH)) {
                periode = 2;
            }
        }else if(Periode3.get(Calendar.YEAR) == currentYear && currentMonth <= Periode3.get(Calendar.MONTH)){
            if(currentMonth < Periode3.get(Calendar.MONTH)){
                periode = 3;
            }else if(Periode3.get(Calendar.MONTH) == currentMonth && currentDay < Periode3.get(Calendar.DAY_OF_MONTH)) {
                periode = 3;
            }
        }else if(Periode4.get(Calendar.YEAR) == currentYear && currentMonth <= Periode4.get(Calendar.MONTH)) {
            if (currentMonth < Periode4.get(Calendar.MONTH)) {
                periode = 4;
            } else if (Periode4.get(Calendar.MONTH) == currentMonth && currentDay < Periode4.get(Calendar.DAY_OF_MONTH)) {
                periode = 4;
            }
        }

        return periode;
    }

    public int maxPuntenPeriode(int period) {
        int maxPunten = 0;
        switch (period) {
            case 1:
                maxPunten = 13;
                break;
            case 2:
                maxPunten = 29;
                break;
            case 3:
                maxPunten = 46;
                break;
            case 4:
                maxPunten = 60;
                break;
        }
        return maxPunten;
    }

    public int missendePunten(){
        int maxBehaaldePunten = maxPuntenPeriode(getCurrentPeriod() - 1);
        int gemistePunten = maxBehaaldePunten - s.getInteger(getActivity(), "CURRENT_ECTS");
        return gemistePunten;
    }

    public String geefStudiepuntenAdvies(){
        int maxPunten = (60 - missendePunten());
        String advies = "";
        if(maxPunten > 50){
            if(currentPeriod == 1){
                if(missendePunten() == 0){
                    advies += "Je bent goed op weg en hebt nog geen studiepunt gemist! Ga zo door in de overige perioden en wie weet heb jij aan het einde van dit jaar je Propedeuse!";
                }else{
                    advies += "Je mist wat studiepunten, maar laat je niet stoppen! Je kan nog gewoon door naar volgend jaar! Zorg wel dat je niet te veel studiepunten gaat missen!";
                }
            }else if(currentPeriod == 2){
                if(missendePunten() == 0){
                    advies += "Je bent goed op weg en hebt nog geen studiepunt gemist! Ga zo door in de overige perioden en wie weet heb jij aan het einde van dit jaar je Propedeuse!";
                }else{
                    advies += "Je mist wat studiepunten, maar laat je niet stoppen! Je kan nog gewoon door naar volgend jaar!";
                }
            }else if(currentPeriod == 3){
                if(missendePunten() == 0){
                    advies += "Je bent goed op weg en hebt nog geen studiepunt gemist! Ga zo door in de overige perioden en wie weet heb jij aan het einde van dit jaar je Propedeuse!";
                }else{
                    advies += "Je mist wat studiepunten, maar laat je niet stoppen!";
                }
            }else if(currentPeriod == 4){
                if(missendePunten() == 0 && s.getInteger(getActivity(), "CURRENT_ECTS") == 60) {
                    advies += "Gefeliciteerd! Je hebt je Propedeuse behaald!!";
                }else if(missendePunten() == 0){
                    advies += "Je bent goed op weg en hebt nog geen studiepunt gemist! Ga zo door in de overige perioden en wie weet heb jij aan het einde van dit jaar je Propedeuse!";
                }
            }

        }else if(maxPunten > 40 && maxPunten < 50){
            if(currentPeriod == 1 || currentPeriod == 2){
                advies += "Je hebt niet genoeg studiepunten om volgend jaar een specialisatie te gaan doen. Bespreek dit advies zo snel mogelijk met je SLB'er!";
            }else if(currentPeriod == 3){
                advies += "Let op! Je hebt niet genoeg studiepunten om volgend jaar een specialisatie te gaan doen. Het is belangrijk dat je wel boven de 40 studiepunten blijft, anders krijg je een BSA (Bindend Studie Advies). Bespreek dit advies zo snel mogelijk met je SLB'er! ";
            }else if(currentPeriod == 4){
                advies += "Je hebt niet genoeg studiepunten om volgend jaar een specialisatie te gaan doen. Je hebt wel genoeg studiepunten om volgend jaar het eerste jaar opnieuw te doen. Je behaalde punten blijven natuurlijk wel staan. Bespreek dit advies wel zo snel mogelijk met je SLB'er!";
            }
        }else{
            advies += "Je kan dit jaar niet meer de 40 studiepunten behalen die nodig zijn om geen BSA (bindend studieadvies) te krijgen. Bespreek dit met je SLB'er om te kijken wat er mogelijk is.";
        }

        return advies;
    }

    public String geefHoofdvakkenAdvies(){
        String tekst = "";
        int behaaldeHoofdvakken = behaaldeHoofdvakken();
        if(currentPeriod == 1) {
            if (behaaldeHoofdvakken == 0) {
                tekst += ", maar er is nog niets aan de hand! Zorg dat je de volgende perioden wel alle verplichte vakken haalt!";
            } else if (behaaldeHoofdvakken == 1) {
                tekst += ", ga zo door!";
            }
        }else if(currentPeriod == 2){
            if (behaaldeHoofdvakken == 0) {
                tekst += ", zorg dat je volgende perioden alle verplichte vakken haalt, anders mag je niet door naar het tweede jaar!";
            } else if (behaaldeHoofdvakken == 1) {
                tekst += ", zorg dat je nog wel minimaal 1 verplicht vak haalt vorige periode!";
            } else if (behaaldeHoofdvakken == 2) {
                tekst += ", ga zo door!!";
            }
        }else if(currentPeriod == 3 || currentPeriod == 4){
            if (behaaldeHoofdvakken == 0) {
                tekst += ", als je aan het einde van deze periode niet twee verplichte vakken heb gehaald mag je niet door naar het tweede jaar.";
            } else if (behaaldeHoofdvakken == 1) {
                tekst += ", als je aan het einde van deze periode niet nog minimaal 1 verplicht vak hebt behaald mag je niet naar het tweede jaar!";
            } else if (behaaldeHoofdvakken == 2) {
                tekst += " en mag volgend jaar op basis van je verplichte vakken door. Zorg wel dat je de rest van de studiepunten haalt!";
            }else if (behaaldeHoofdvakken == 3) {
                tekst += ", gefeliciteerd! Je hebt minimaal 2 verplichte vakken gehaald. Op basis van je verplichte vakken ben je dus door naar het tweede jaar.";
            }else if (behaaldeHoofdvakken == 4) {
                tekst += ", gefeliciteerd! Je hebt alle verplichte vakken gehaald!";
            }
        }
        return tekst;
    }

    public int behaaldeHoofdvakken(){
        List<String> hoofdVakken = Arrays.asList("IOPR1", "IOPR2", "IRDB", "INET");

        double cijfer = 0.0;
        int behaaldeVakken = 0;

        try {
            rs.moveToFirst();

            if (rs != null && rs.moveToFirst()) {
                for (int i = 0; i < 19; i++) {
                    //Als toets een hoofdvak is
                    if (!rs.isLast()) {
                        String toets = rs.getString(rs.getColumnIndex("name"));
                        if (hoofdVakken.contains(toets)) {
                            //Vraag het cijfer op van het hoofdvak
                            cijfer = Double.parseDouble(rs.getString(rs.getColumnIndex("grade")));
                            //Als het cijfer hoger is dan 5.5 vak behaald
                            if (cijfer >= 5.5) {
                                behaaldeVakken += 1;
                            }
                        }
                        rs.moveToNext();
                    }
                }
            }
            return behaaldeVakken;
        }catch(Exception e){
            return 0;
        }
    }

    public String medtSpec(){
        String tekst = "";
        if(getCijferVak("IWDR") < 5.5){
            tekst += "Je hebt IWDR niet behaald en mag daarom niet de specialisatie Mediatechnologie volgen. Hou hier rekening mee in de keuze van je specialisatie.";
        }else if(getCijferVak("IWDR") >= 5.5){
            tekst += "Gefeliciteerd! Je hebt IWDR behaald en mag daarom de specialisatie Mediatechnologie volgen!";
        }else if(getCijferVak("IWDR") == 0.0){
            tekst += "Je hebt nog geen cijfer voor IWDR ingevoerd. Voer eerst een cijfer in om advies te kunnen ontvangen!";
        }

        tekst += "\n";
        return tekst;
    }

    public String bdamSpec(){
        String tekst = "";
        if(getCijferVak("IIBUI") < 5.5){
            tekst += "Je hebt IIBUI niet behaald en mag daarom niet de specialisatie Business en Data Management volgen. Hou hier rekening mee in de keuze van je specialisatie.";
        }else if(getCijferVak("IIBUI") >= 5.5){
            tekst += "Gefeliciteerd! Je hebt IIBUI behaald en mag daarom de specialisatie Business en Data Management volgen!";
        }else if(getCijferVak("IIBUI") == 0.0){
             tekst += "Je hebt nog geen cijfer voor IIBUI ingevoerd. Voer eerst een cijfer in om advies te kunnen ontvangen!";
         }
        tekst += "\n";
        return tekst;
    }

    public String fictSpec(){
        String tekst = "";
        if(getCijferVak("IFIT") < 5.5){
            tekst += "Je hebt IFIT niet behaald en mag daarom niet de specialisatie Forensisch ICT volgen. Hou hier rekening mee in de keuze van je specialisatie.";
        }else if(getCijferVak("IMUML") >= 5.5){
            tekst += "Gefeliciteerd! Je hebt IFIT behaald en mag daarom de specialisatie Forensisch ICT volgen!";
        }else if(getCijferVak("IFIT") == 0.0) {
            tekst += "Je hebt nog geen cijfer voor IFIT ingevoerd. Voer eerst een cijfer in om advies te kunnen ontvangen!";
        }

        tekst += "\n";
        return tekst;
    }

    public String seSpec(){
        String tekst = "";
        double umlCijfer = getCijferVak("IMUML");
        Log.e("umlCijfer", "" + umlCijfer);
        if(umlCijfer < 5.5){
            tekst += "Je hebt IMUML niet behaald en mag daarom niet de specialisatie Software Engineering volgen. Hou hier rekening mee in de keuze van je specialisatie.";
        }else if(getCijferVak("IMUML") >= 5.5){
            tekst += "Gefeliciteerd! Je hebt IMUML behaald en mag daarom de specialisatie Software Engineering volgen!";
        }else if(getCijferVak("IMUML") == 0.0){
            tekst += "Je hebt nog geen cijfer voor IMUML ingevoerd. Voer eerst een cijfer in om advies te kunnen ontvangen!";
        }
        return tekst;
    }

    public double getCijferVak(String vak){
        double cijfer = 0.0;
        try {
            rs.moveToFirst();
            for (int i = 0; i < 19; i++) {
                rs.moveToNext();
                String toets = rs.getString(rs.getColumnIndex("name"));
                if (vak.equals(toets)) {
                    cijfer = Double.parseDouble(rs.getString(rs.getColumnIndex("grade")));
                    break;
                }
            }
            return cijfer;
        }catch(Exception e){
            return 0.0;
        }
    }
}
