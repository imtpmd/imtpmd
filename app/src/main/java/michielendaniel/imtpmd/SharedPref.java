package michielendaniel.imtpmd;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.io.Serializable;

/**
 * Created by Dell on 15-4-2016.
 */
public class SharedPref implements Serializable{
    private boolean FIRST = true;
    private String NAME = "Naam";
    private int CURRENT_ECTS = 0;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public void saveString(Context context, String text, String name){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();

        editor.putString(name, text);
        editor.apply();
    }

    public String getString(Context context, String name){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(name, "");
    }

    public void saveBoolean(Context context, Boolean value, String name){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();

        editor.putBoolean(name, value);
        editor.apply();
    }

    public Boolean getBoolean(Context context, String name){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(name, true);
    }

    public void saveInteger(Context context, int value, String name){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();

        editor.putInt(name, value);
        editor.apply();
    }

    public int getInteger(Context context, String name){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(name, 0);
    }
}


