package michielendaniel.imtpmd.Model;

import java.io.Serializable;

/**
 * Created by Michiel on 15-4-2016.
 */
public class Course implements Serializable {           // WAAROM serializable ????

    public String name;
    public String ects;
    public String grade;
    public String period;


    public Course(String courseName, String ects, String grade, String period){
        this.name = courseName;
        this.ects = ects;
        this.grade = grade;
        this.period = period;
    }

    // ADD GETTERS
    public String getCourseName() {
        return this.name;
    }

    public String getEcts() {
        return this.ects;
    }

    public String getGrade() {
        return this.grade;
    }

    public String getPeriod() {
        return this.period;
    }

    // ADD SETTERS
}
