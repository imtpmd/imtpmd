package michielendaniel.imtpmd.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import michielendaniel.imtpmd.Model.Course;
import michielendaniel.imtpmd.R;

/**
 * Created by Michiel on 15-4-2016.
 */
public class CourseListAdapter extends ArrayAdapter<Course> {

    public CourseListAdapter(Context context, int resource, List<Course> objects){
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;

        if (convertView == null ) {
            vh = new ViewHolder();
            LayoutInflater li = LayoutInflater.from(getContext());
            convertView = li.inflate(R.layout.view_content_row, parent, false);
            vh.name = (TextView) convertView.findViewById(R.id.subject_name);
            vh.code = (TextView) convertView.findViewById(R.id.subject_code);
            vh.grade = (TextView) convertView.findViewById(R.id.subject_grade);
            vh.arrow = (ImageView) convertView.findViewById(R.id.subject_arrow);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        Course cm = getItem(position);
        vh.name.setText("Vak: " + cm.name);
        vh.code.setText("EC: " + cm.ects);
        vh.grade.setText("Cijfer: " + cm.grade);
        vh.arrow.setImageResource(R.drawable.ic_navigate_next_black_36dp);
        return convertView;
    }

    private static class ViewHolder {
        TextView name;
        TextView code;
        TextView grade;
        ImageView arrow;

    }
}

