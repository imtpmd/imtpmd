package michielendaniel.imtpmd;

import android.app.FragmentManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import michielendaniel.imtpmd.Database.DatabaseHelper;
import michielendaniel.imtpmd.Database.DatabaseInfo;
import michielendaniel.imtpmd.Fragments.ProgressFragment;

public class CourseActivity extends AppCompatActivity {

    TextView courseSummary, courseEcts, courseCore, courseSpecialization;
    EditText enterGrade;
    Button saveGrade;
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        courseSummary = (TextView) findViewById(R.id.courseSummary);
        courseEcts = (TextView) findViewById(R.id.courseEcts);
        courseCore = (TextView) findViewById(R.id.courseCore);
        courseSpecialization = (TextView) findViewById(R.id.courseSpecialization);

        Intent intent = getIntent();
        final int position_id = intent.getIntExtra("position", 0);

        switch(position_id) {
            case 0:
                getSupportActionBar().setTitle("IARCH");
                courseSummary.append(" IARCH");
                courseEcts.append(" 3");
                courseCore.append(" Nee");
                courseSpecialization.append(" Nee");
                break;
            case 1:
                getSupportActionBar().setTitle("IIBPM");
                courseSummary.append(" IIBPM");
                courseEcts.append(" 3");
                courseCore.append(" Nee");
                courseSpecialization.append(" Nee");
                break;
            case 2:
                getSupportActionBar().setTitle("IHBO");
                courseSummary.append(" IHBO");
                courseEcts.append(" 3");
                courseCore.append(" Nee");
                courseSpecialization.append(" Nee");
                break;
            case 3:
                getSupportActionBar().setTitle("IOPR1");
                courseSummary.append(" IOPR1");
                courseEcts.append(" 4");
                courseCore.append(" Ja");
                courseSpecialization.append(" Nee");
                break;
            case 4:
                getSupportActionBar().setTitle("INET");
                courseSummary.append(" INET");
                courseEcts.append(" 3");
                courseCore.append(" Ja");
                courseSpecialization.append(" Nee");
                break;
            case 5:
                getSupportActionBar().setTitle("IWDR");
                courseSummary.append(" IWDR");
                courseEcts.append(" 3");
                courseCore.append(" Nee");
                courseSpecialization.append(" Ja");
                break;
            case 6:
                getSupportActionBar().setTitle("IRDB");
                courseSummary.append(" IRDB");
                courseEcts.append(" 3");
                courseCore.append(" Ja");
                courseSpecialization.append(" Nee");
                break;
            case 7:
                getSupportActionBar().setTitle("IIBUI");
                courseSummary.append(" IIBUI");
                courseEcts.append(" 3");
                courseCore.append(" Nee");
                courseSpecialization.append(" Ja");
                break;
            case 8:
                getSupportActionBar().setTitle("IPRODAM");
                courseSummary.append(" IPRODAM");
                courseEcts.append(" 2");
                courseCore.append(" Nee");
                courseSpecialization.append(" Nee");
                break;
            case 9:
                getSupportActionBar().setTitle("IPROMEDT");
                courseSummary.append(" IPROMEDT");
                courseEcts.append(" 2");
                courseCore.append(" Nee");
                courseSpecialization.append(" Nee");
                break;
            case 10:
                getSupportActionBar().setTitle("IMUML");
                courseSummary.append(" IMUML");
                courseEcts.append(" 3");
                courseCore.append(" Nee");
                courseSpecialization.append(" Ja");
                break;
            case 11:
                getSupportActionBar().setTitle("IOPR2");
                courseSummary.append(" IOPR2");
                courseEcts.append(" 4");
                courseCore.append(" Ja");
                courseSpecialization.append(" Nee");
                break;
            case 12:
                getSupportActionBar().setTitle("IFIT");
                courseSummary.append(" IFIT");
                courseEcts.append(" 3");
                courseCore.append(" Nee");
                courseSpecialization.append(" Ja");
                break;
            case 13:
                getSupportActionBar().setTitle("IPOFIT");
                courseSummary.append(" IPOFIT");
                courseEcts.append(" 2");
                courseCore.append(" Nee");
                courseSpecialization.append(" Nee");
                break;
            case 14:
                getSupportActionBar().setTitle("IPOSE");
                courseSummary.append(" IPOSE");
                courseEcts.append(" 2");
                courseCore.append(" Nee");
                courseSpecialization.append(" Nee");
                break;
            case 15:
                getSupportActionBar().setTitle("IIPXXXX");
                courseSummary.append(" IIPXXXX");
                courseEcts.append(" 10");
                courseCore.append(" Nee");
                courseSpecialization.append(" Ja");
                break;
            case 16:
                getSupportActionBar().setTitle("IPROV");
                courseSummary.append(" IPROV");
                courseEcts.append(" 3");
                courseCore.append(" Nee");
                courseSpecialization.append(" Nee");
                break;
            case 17:
                getSupportActionBar().setTitle("ICOMMP");
                courseSummary.append(" ICOMMP");
                courseEcts.append(" 3");
                courseCore.append(" Nee");
                courseSpecialization.append(" Nee");
                break;
            case 18:
                getSupportActionBar().setTitle("ISLP");
                courseSummary.append(" ISLP");
                courseEcts.append(" 1");
                courseCore.append(" Nee");
                courseSpecialization.append(" Nee");
                break;
        }

        enterGrade = (EditText) findViewById(R.id.enterGrade);
        saveGrade = (Button) findViewById(R.id.saveGrade);

        saveGrade.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                try {
                    if (Double.parseDouble(enterGrade.getText().toString()) <= 10.0 && Double.parseDouble(enterGrade.getText().toString()) >= 1.0) {
                        saveGrade(v, position_id);
                        Snackbar.make(v, R.string.gradeAdjusted, Snackbar.LENGTH_SHORT).show();
                    } else {
                        Snackbar.make(v, R.string.gradeIncorrect, Snackbar.LENGTH_SHORT).show();
                    }
                } catch(NumberFormatException e) {
                    Snackbar.make(v, R.string.gradeMissing, Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void saveGrade (View v, int position_id) {
        DatabaseHelper dbHelper = DatabaseHelper.getHelper(this);
        Cursor cursor = dbHelper.query(DatabaseInfo.CourseTables.COURSE, new String[] {"*"}, null, null, null, null, null);

        cursor.moveToFirst();
        cursor.move(position_id);

        Log.e("Position", ""+position_id);

        String name = (String) cursor.getString(cursor.getColumnIndex("name"));
        String ects = (String) cursor.getString(cursor.getColumnIndex("ects"));
        String grade = (String) cursor.getString(cursor.getColumnIndex("grade"));
        String period = (String) cursor.getString(cursor.getColumnIndex("period"));

        String newGrade = enterGrade.getText().toString();

        dbHelper.updateRow(name, ects, newGrade, period, position_id);
        Intent i = new Intent(CourseActivity.this, MainActivity.class);
        i.putExtra("move", true);
        startActivity(i);

        //cursor.move(position_id);
    }

}
